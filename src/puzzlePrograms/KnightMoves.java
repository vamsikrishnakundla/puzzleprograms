package puzzlePrograms;

import java.util.Scanner;

public class KnightMoves {

	int count = 0;

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int inputRow = sc.nextInt();
		int inputColumn = sc.nextInt();

		System.out
				.println("Possible moves of KNIGHT for given position is: " + getNumberOfMoves(inputRow, inputColumn));

	}

	public static int getNumberOfMoves(int inputRow, int inputColumn) {

		int possibleMoves[][] = { { 2, 1 }, { 1, 2 }, { -1, 2 }, { -2, 1 }, { -2, -1 }, { -1, -2 }, { 1, -2 },{ 2, -1 } };
		int count = 0;

		for (int i = 0; i < possibleMoves.length; i++) {
			for (int j = 0; j < possibleMoves[i].length; j++) {
				
				int x = inputRow + possibleMoves[i][j];
				j++;
				int y = inputColumn + possibleMoves[i][j];
				
				if (x >= 0 && y >= 0 && x < 8 && y < 8)
					count++;

			}
		}

		return count;

	}

}
